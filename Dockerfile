ARG GLANCES_VER=ubuntu-3.4.0.3-full
ARG DEBIAN_VER=bullseye-20230919-slim

FROM debian:${DEBIAN_VER} as hddtemp_source

RUN apt update

RUN apt install hddtemp -y

FROM nicolargo/glances:${GLANCES_VER}

COPY --from=hddtemp_source /etc/hddtemp.db /etc/hddtemp.db

COPY --from=hddtemp_source /usr/sbin/hddtemp ./hddtemp

COPY wrapper.sh ./wrapper.sh

#hddtemp port
EXPOSE 7634

CMD ["./wrapper.sh"]
