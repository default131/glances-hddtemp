#!/usr/bin/env bash

if [ -z "$DRIVES" ]; then
  DRIVES=$(lsblk --noheadings --raw | awk '$6 == "disk"' | awk '{print $1 }' | sed 's%^%/dev/%')
fi

./hddtemp $HDDTEMP_OPT -d $DRIVES 2>&1

echo "DRIVES:"
echo "$DRIVES"

/venv/bin/python3 -m glances -C /etc/glances.conf $GLANCES_OPT 2>&1
